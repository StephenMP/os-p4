Stephen Porter
CS 453
Project 4 : Booga Driver
Updated 11/20/2014

=======
Details
=======
To Build/Load:
	/* I modified the load script to run make and load module */
	./booga_load

To Run:
	./test-booga <driver> <bytes> <read|write>

To Clean/Unload
	/* I modified the unload script to run make clean and unload module */
	./booga_unload

=====
Files
=====
booga.c		//Source code
booga.h		//Source code header
booga_load	//Load script to build and load module (runs make)
booga_unload	//Unload script to clean and unload module (runs make clean)
full_test	//My script to run a full test included memory leak check
Makefile
README.txt
test-booga	//Test program
test-booga.c	//Test source

======
Coupon
======
So, I thought the due date was the 20th (For some reason) and I forgot that it was the 19th.
So, even though I was having Onyx troubles which were fixed today (Marty rocks!) it would
have ended up being a day late. Because of this, I don't feel that it would be ethical for
me to not use 1 day of my coupon.

Thus, I would like to use 1 day of the 4 days I have leaving me with 3 days on my coupon.

==========
Discussion
==========
This was a pretty straighforward assignment. I just went off of example 4 which included the
stuff we needed to write to the proc using seq_files. From there it was pretty simple to
construct a struct to hold the data we needed and fill in the read/write/open/init/exit
functions.

The hardest part was copying the strings to my phrase_buffer. By hardest, I just mean it was
something that took more than 2 seconds of thought haha. I just came up with a straight 1-to-1
copy using indexes between the output string and my phrase_buffer. Not sure if there was a
better way to do it or not.

The other minor kink was how to generate a random number. A quick search and read through on
some documentation pointed me in the direction of get_random_bytes to generate random numbers
in the kernel, though, it is not the best random number generator and there are better
psuedo-random algorithms, but we aren't trying to be super random here.

Also, I found a memory leak in the test-booga code. Basically, in the main method, we allocate
memory for the drivers in line 81, but this is never freed prior to exit. Thus, we lose 20
bytes of memory (according to valgrind).
