
#ifndef __BOOGA_H
#define __BOOGA_H

#ifndef BOOGA_MAJOR
#define BOOGA_MAJOR 0
#endif

#ifndef BOOGA_NR_DEVS
#define BOOGA_NR_DEVS 4
#endif

#ifndef BOOGA_PHRASES
#define BOOGA_PHRASES 4
#endif

#define BOOGA_INDEX 0
#define GOOGOO_INDEX 1
#define NEKA_INDEX 2
#define WOOGA_INDEX 3

/* Split minors in two parts */
#define TYPE(dev)   (MINOR(dev) >> 4)  /* high nibble */
#define NUM(dev)    (MINOR(dev) & 0xf) /* low  nibble */

/*
 * The different configurable parameters
 */
struct booga_stats {
    int minor;
	int bytes_read;
	int bytes_written;
	int num_device_opens[BOOGA_NR_DEVS];
	int num_phrase_chosen[BOOGA_PHRASES];
	char *phrase_buffer;
	struct semaphore sem;
};

typedef struct booga_stats booga_stats;

/* Booga helper functions */
static int generateIndex(void);
static char *getPhrase(int);
static void populatePhraseBuffer(int, int);

#endif /* __BOOGA_H */
