/* Includes */
#include <linux/seq_file.h>
#include <linux/version.h>
#include <linux/proc_fs.h>
#include <linux/random.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include "booga.h"

/* Local Module Params */
static int booga_nr_devs = BOOGA_NR_DEVS;
static int booga_major = BOOGA_MAJOR;
module_param(booga_nr_devs, int, 0);
module_param(booga_major, int, 0);

/* Module Data */
MODULE_AUTHOR("Stephen Porter");
MODULE_LICENSE("GPL v2");

/* Local device statistics struct */
static booga_stats *booga_device_stats;

/* Proc file struct for proc output */
static struct proc_dir_entry* booga_proc_file;

/* Method preprocessors */
static ssize_t booga_write(struct file *, const char *, size_t , loff_t *);
static ssize_t booga_read(struct file *, char *, size_t , loff_t *);
static int booga_release(struct inode *, struct file *);
static int booga_open(struct inode *, struct file *);

/* File operations mapping */
static struct file_operations booga_fops = {
    read:       booga_read,
    write:      booga_write,
    open:       booga_open,
    release:    booga_release,
};

/**
 * Function to handle device open
 */
static int booga_open(struct inode *inode, struct file *filp)
{
    /* Get device # */
	int num = NUM(inode->i_rdev);

    /* Handle error */
	if(num >= booga_nr_devs)
        return -ENODEV;

    /* Assign our file operations mappings */
	filp->f_op = &booga_fops;

    /* Throw down semaphore */
	if(down_interruptible(&booga_device_stats->sem))
		return (-ERESTARTSYS);

    /* Update device statistics */
	booga_device_stats->minor = num;
	booga_device_stats->num_device_opens[num]++;
	printk("<1> /dev/booga%d opened\n", num);

	up(&booga_device_stats->sem);

	try_module_get(THIS_MODULE);
	return 0;
}

/**
 * Function to handle device release
 */
static int booga_release(struct inode *inode, struct file *filp)
{
	if(down_interruptible(&booga_device_stats->sem))
		return (-ERESTARTSYS);

	up(&booga_device_stats->sem);

	module_put(THIS_MODULE);
	return (0);
}

/**
 * Function to handle device read
 */
static ssize_t booga_read(struct file *filp, char *buf, size_t count, loff_t *f_pos)
{
	int phrase_index;

	printk("<1>booga_read invoked.\n");
	if(down_interruptible(&booga_device_stats->sem))
		return (-ERESTARTSYS);

    /* Initialize our phrase buffer */
	booga_device_stats->phrase_buffer = (char *) kmalloc(sizeof(char) * count, GFP_KERNEL);

	/* Handle malloc errors */
	if(!booga_device_stats->phrase_buffer)
		return -ENOMEM;

    /* Get a random number between 0 and 3 included */
	phrase_index = generateIndex();

	/* Fill our phrase buffer */
	populatePhraseBuffer(phrase_index, count);

    /* Copy to our user space using the populated phrase and handle error */
	if(__copy_to_user(buf, booga_device_stats->phrase_buffer, count) > 0)
		printk("booga failed to copy bytes to user space");
	else {
        /* Updated data upon success */
		booga_device_stats->num_phrase_chosen[phrase_index]++;
		booga_device_stats->bytes_read += sizeof(char) * count;
	}

	/* Keep it clean */
	kfree(booga_device_stats->phrase_buffer);

	up(&booga_device_stats->sem);

	return count;
}

/**
 * Function to handle device write
 */
static ssize_t booga_write(struct file *filp, const char *buf, size_t count , loff_t *f_pos)
{
	printk("<1>booga_write invoked.\n");
	if(down_interruptible(&booga_device_stats->sem))
		return (-ERESTARTSYS);

    /* Just pretend to write stuff. If 3 is passed, we terminate using SIGTERM */
	if(booga_device_stats->minor == 3)
		send_sig(SIGTERM, current, 0);
	else
		booga_device_stats->bytes_written += sizeof(char) * count;

	up(&booga_device_stats->sem);
	return count;
}

/**
 * Initialize our device statistics we are tracking
 */
static void init_booga_device_stats(void)
{
	booga_device_stats->minor = 0;
	booga_device_stats->bytes_read = 0;
	booga_device_stats->bytes_written = 0;

	booga_device_stats->num_device_opens[0] = 0;
	booga_device_stats->num_device_opens[1] = 0;
	booga_device_stats->num_device_opens[2] = 0;
	booga_device_stats->num_device_opens[3] = 0;

	booga_device_stats->num_phrase_chosen[BOOGA_INDEX] = 0;
	booga_device_stats->num_phrase_chosen[GOOGOO_INDEX] = 0;
	booga_device_stats->num_phrase_chosen[NEKA_INDEX] = 0;
	booga_device_stats->num_phrase_chosen[WOOGA_INDEX] = 0;

	sema_init(&booga_device_stats->sem, 1);
}

/**
 * The output to proc/driver/booga to display on cat
 */
static int booga_proc_show(struct seq_file *m, void *v)
{
	seq_printf(m, "bytes read = %d\n", booga_device_stats->bytes_read);
	seq_printf(m, "bytes written = %d\n", booga_device_stats->bytes_written);
	seq_printf(m, "number of opens:\n");
	seq_printf(m, "\t/dev/booga0 = %d times\n", booga_device_stats->num_device_opens[0]);
	seq_printf(m, "\t/dev/booga1 = %d times\n", booga_device_stats->num_device_opens[1]);
	seq_printf(m, "\t/dev/booga2 = %d times\n", booga_device_stats->num_device_opens[2]);
	seq_printf(m, "\t/dev/booga3 = %d times\n", booga_device_stats->num_device_opens[3]);
	seq_printf(m, "strings output:\n");
	seq_printf(m, "\t booga! booga! = %d times\n", booga_device_stats->num_phrase_chosen[BOOGA_INDEX]);
	seq_printf(m, "\t googoo! gaagaa! = %d times\n", booga_device_stats->num_phrase_chosen[GOOGOO_INDEX]);
    seq_printf(m, "\t wooga! wooga! = %d times\n", booga_device_stats->num_phrase_chosen[WOOGA_INDEX]);
	seq_printf(m, "\t neka! maka! = %d times\n", booga_device_stats->num_phrase_chosen[NEKA_INDEX]);

	return 0;
}

/**
 * Handle the initialization of seq_file open for the /proc/driver/booga
 */
static int booga_proc_open(struct inode *inode, struct file *file)
{
	return single_open(file, booga_proc_show, NULL);
}

/**
 * file operations struct to manage proc comm
 */
static const struct file_operations booga_proc_fops = {
	.owner	= THIS_MODULE,
	.open	= booga_proc_open,
	.read	= seq_read,
	.llseek	= seq_lseek,
	.release	= single_release,
};

/**
 * Module initialization
 */
static __init int booga_init(void)
{
	int result;

	/* Register your major, and accept a dynamic number */
	result = register_chrdev(booga_major, "booga", &booga_fops);
	if(result < 0) {
		printk(KERN_WARNING "booga: can't get major %d\n", booga_major);
		return result;
	}
	if(booga_major == 0) booga_major = result;  /* dynamic */
	printk("<1> booga device driver version 4: loaded at major number %d\n", booga_major);

	booga_device_stats = (booga_stats *) kmalloc(sizeof(booga_stats), GFP_KERNEL);
	if(!booga_device_stats) {
		result = -ENOMEM;
		goto fail_malloc;
	}
	init_booga_device_stats();

	/* We assume that the /proc/driver exists. Otherwise we need to use proc_mkdir to
	 * create it as follows: proc_mkdir("driver", NULL);
	 */
	booga_proc_file = proc_create("driver/booga", 0, NULL, &booga_proc_fops);
	if(!booga_proc_file)  {
		result = -ENOMEM;
		goto fail_malloc;
	}

	return 0;

fail_malloc:
	unregister_chrdev(booga_major, "booga");
	return  result;
}

/**
 * Module exit
 */
static __exit  void booga_cleanup(void)
{
	remove_proc_entry("driver/booga", NULL /* parent dir */);
	kfree(booga_device_stats);
	unregister_chrdev(booga_major, "booga");
	printk("<1> booga device driver: unloaded\n");
}

/**********************/
/** Helper Functions **/
/**********************/

/**
 * Generates a random number between 0 and 3
 */
static int generateIndex()
{
	int val;

	get_random_bytes(&val, sizeof(val));
	val = val % BOOGA_PHRASES;

	if(val < 0)
        val = val * -1;

	return val;
}

/**
 * Picks a phrase based on a random number
 * between 0 and 3 included
 */
static char * getPhrase(int phrase_index)
{
	switch(phrase_index) {
		case BOOGA_INDEX:
		    return "booga! booga!";
		case GOOGOO_INDEX:
		    return "googoo! gaagaa!";
		case NEKA_INDEX:
		    return "neka! maka!";
		case WOOGA_INDEX:
		    return "wooga! wooga!";
	}
	return NULL;
}

/**
 * Builds the phrase based on amount of bytes
 * to read.
 */
static void populatePhraseBuffer(int phrase_index, int count)
{
	int i, j;
	char *phrase;

	phrase = getPhrase(phrase_index);

    j=0;
	for(i = 0; i < count; i++) {
		if(phrase[j] != '\0'){
			booga_device_stats->phrase_buffer[i] = phrase[j];
			j++;
		}
		else{
			booga_device_stats->phrase_buffer[i] = ' ';
			j=0;
		}
	}
}

module_init(booga_init);
module_exit(booga_cleanup);

/* vim: set ts=4: */
